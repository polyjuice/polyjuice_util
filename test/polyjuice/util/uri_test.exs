# Copyright 2021 Hubert Chathi <hubert@uhoreg.ca>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-FileCopyrightText: 2021 Hubert Chathi <hubert@uhoreg.ca>
# SPDX-License-Identifier: Apache-2.0

defmodule Polyjuice.Util.URITest do
  use ExUnit.Case
  doctest Polyjuice.Util.URI

  test "parses mxc: URIs" do
    assert Polyjuice.Util.URI.parse("mxc://uhoreg.ca/JbcxMQHvPoPUoRkwQRdmwXKm") ==
             {:ok,
              %Polyjuice.Util.URI{
                type: :media,
                identifier: {"uhoreg.ca", "JbcxMQHvPoPUoRkwQRdmwXKm"}
              }}
  end

  test "parses matrix.to URIs" do
    # test both URL encoded and unencoded
    # room ID
    assert Polyjuice.Util.URI.parse("https://matrix.to/#/%21OGEhHVWSdvArJzumhm%3amatrix.org") ==
             {:ok,
              %Polyjuice.Util.URI{
                type: :room_id,
                identifier: "!OGEhHVWSdvArJzumhm:matrix.org"
              }}

    assert Polyjuice.Util.URI.parse("https://matrix.to/#/!OGEhHVWSdvArJzumhm:matrix.org") ==
             {:ok,
              %Polyjuice.Util.URI{
                type: :room_id,
                identifier: "!OGEhHVWSdvArJzumhm:matrix.org"
              }}

    # room alias
    assert Polyjuice.Util.URI.parse("https://matrix.to/#/%23matrix%3amatrix.org") ==
             {:ok,
              %Polyjuice.Util.URI{
                type: :room_alias,
                identifier: "#matrix:matrix.org"
              }}

    assert Polyjuice.Util.URI.parse("https://matrix.to/#/#matrix:matrix.org") ==
             {:ok,
              %Polyjuice.Util.URI{
                type: :room_alias,
                identifier: "#matrix:matrix.org"
              }}

    # events
    assert Polyjuice.Util.URI.parse(
             "https://matrix.to/#/%21OGEhHVWSdvArJzumhm%3amatrix.org/%24eventid%2F%2B"
           ) ==
             {:ok,
              %Polyjuice.Util.URI{
                type: :event,
                identifier: {"!OGEhHVWSdvArJzumhm:matrix.org", "$eventid/+"}
              }}

    assert Polyjuice.Util.URI.parse(
             "https://matrix.to/#/!OGEhHVWSdvArJzumhm:matrix.org/$eventid/+"
           ) ==
             {:ok,
              %Polyjuice.Util.URI{
                type: :event,
                identifier: {"!OGEhHVWSdvArJzumhm:matrix.org", "$eventid/+"}
              }}

    assert Polyjuice.Util.URI.parse("https://matrix.to/#/%23matrix%3amatrix.org/%24eventid%2F%2B") ==
             {:ok,
              %Polyjuice.Util.URI{
                type: :event_by_room_alias,
                identifier: {"#matrix:matrix.org", "$eventid/+"}
              }}

    assert Polyjuice.Util.URI.parse("https://matrix.to/#/#matrix:matrix.org/$eventid/+") ==
             {:ok,
              %Polyjuice.Util.URI{
                type: :event_by_room_alias,
                identifier: {"#matrix:matrix.org", "$eventid/+"}
              }}
  end

  test "parses matrix URIs" do
    # test both URL encoded and unencoded
    # room ID
    assert Polyjuice.Util.URI.parse(
             "matrix:roomid/OGEhHVWSdvArJzumhm%3amatrix.org?via=matrix.org"
           ) ==
             {:ok,
              %Polyjuice.Util.URI{
                type: :room_id,
                identifier: "!OGEhHVWSdvArJzumhm:matrix.org",
                query_params: [{"via", "matrix.org"}]
              }}

    assert Polyjuice.Util.URI.parse("matrix:roomid/OGEhHVWSdvArJzumhm:matrix.org?via=matrix.org") ==
             {:ok,
              %Polyjuice.Util.URI{
                type: :room_id,
                identifier: "!OGEhHVWSdvArJzumhm:matrix.org",
                query_params: [{"via", "matrix.org"}]
              }}

    # room alias
    assert Polyjuice.Util.URI.parse("matrix:r/matrix%3amatrix.org") ==
             {:ok,
              %Polyjuice.Util.URI{
                type: :room_alias,
                identifier: "#matrix:matrix.org"
              }}

    assert Polyjuice.Util.URI.parse("matrix:r/matrix:matrix.org") ==
             {:ok,
              %Polyjuice.Util.URI{
                type: :room_alias,
                identifier: "#matrix:matrix.org"
              }}

    # events
    assert Polyjuice.Util.URI.parse(
             "matrix:roomid/OGEhHVWSdvArJzumhm%3amatrix.org/e/eventid%2F%2B?via=matrix.org"
           ) ==
             {:ok,
              %Polyjuice.Util.URI{
                type: :event,
                identifier: {"!OGEhHVWSdvArJzumhm:matrix.org", "$eventid/+"},
                query_params: [{"via", "matrix.org"}]
              }}

    assert Polyjuice.Util.URI.parse(
             "matrix:roomid/OGEhHVWSdvArJzumhm:matrix.org/e/eventid%2f%2b?via=matrix.org"
           ) ==
             {:ok,
              %Polyjuice.Util.URI{
                type: :event,
                identifier: {"!OGEhHVWSdvArJzumhm:matrix.org", "$eventid/+"},
                query_params: [{"via", "matrix.org"}]
              }}

    assert Polyjuice.Util.URI.parse("matrix:r/matrix%3amatrix.org/e/eventid%2F%2B") ==
             {:ok,
              %Polyjuice.Util.URI{
                type: :event_by_room_alias,
                identifier: {"#matrix:matrix.org", "$eventid/+"}
              }}

    assert Polyjuice.Util.URI.parse("matrix:r/matrix:matrix.org/e/eventid%2f%2b") ==
             {:ok,
              %Polyjuice.Util.URI{
                type: :event_by_room_alias,
                identifier: {"#matrix:matrix.org", "$eventid/+"}
              }}
  end

  test "generates mxc URI" do
    assert Polyjuice.Util.URI.to_string(%Polyjuice.Util.URI{
             type: :media,
             identifier: {"uhoreg.ca", "JbcxMQHvPoPUoRkwQRdmwXKm"}
           }) == "mxc://uhoreg.ca/JbcxMQHvPoPUoRkwQRdmwXKm"
  end

  test "generates matrix.to URIs" do
    assert Polyjuice.Util.URI.to_string(
             %Polyjuice.Util.URI{
               type: :room_id,
               identifier: "!OGEhHVWSdvArJzumhm:matrix.org"
             },
             :matrix_to
           ) == "https://matrix.to/#/%21OGEhHVWSdvArJzumhm%3Amatrix.org"

    assert Polyjuice.Util.URI.to_string(
             %Polyjuice.Util.URI{
               type: :room_alias,
               identifier: "#matrix:matrix.org"
             },
             :matrix_to
           ) == "https://matrix.to/#/%23matrix%3Amatrix.org"

    assert Polyjuice.Util.URI.to_string(
             %Polyjuice.Util.URI{
               type: :event,
               identifier: {"!OGEhHVWSdvArJzumhm:matrix.org", "$eventid/+"},
               query_params: [{"via", "matrix.org"}]
             },
             :matrix_to
           ) ==
             "https://matrix.to/#/%21OGEhHVWSdvArJzumhm%3Amatrix.org/%24eventid%2F%2B?via=matrix.org"

    assert Polyjuice.Util.URI.to_string(
             %Polyjuice.Util.URI{
               type: :event_by_room_alias,
               identifier: {"#matrix:matrix.org", "$eventid/+"}
             },
             :matrix_to
           ) == "https://matrix.to/#/%23matrix%3Amatrix.org/%24eventid%2F%2B"
  end

  test "generates matrix URIs" do
    assert Polyjuice.Util.URI.to_string(
             %Polyjuice.Util.URI{
               type: :room_id,
               identifier: "!OGEhHVWSdvArJzumhm:matrix.org"
             },
             :matrix
           ) == "matrix:roomid/OGEhHVWSdvArJzumhm%3Amatrix.org"

    assert Polyjuice.Util.URI.to_string(
             %Polyjuice.Util.URI{
               type: :room_alias,
               identifier: "#matrix:matrix.org"
             },
             :matrix
           ) == "matrix:r/matrix%3Amatrix.org"

    assert Polyjuice.Util.URI.to_string(
             %Polyjuice.Util.URI{
               type: :event,
               identifier: {"!OGEhHVWSdvArJzumhm:matrix.org", "$eventid/+"},
               query_params: [{"via", "matrix.org"}]
             },
             :matrix
           ) == "matrix:roomid/OGEhHVWSdvArJzumhm%3Amatrix.org/e/eventid%2F%2B?via=matrix.org"

    assert Polyjuice.Util.URI.to_string(
             %Polyjuice.Util.URI{
               type: :event_by_room_alias,
               identifier: {"#matrix:matrix.org", "$eventid/+"}
             },
             :matrix
           ) == "matrix:r/matrix%3Amatrix.org/e/eventid%2F%2B"
  end

  test "to_string" do
    assert to_string(%Polyjuice.Util.URI{
             type: :media,
             identifier: {"uhoreg.ca", "JbcxMQHvPoPUoRkwQRdmwXKm"}
           }) == "mxc://uhoreg.ca/JbcxMQHvPoPUoRkwQRdmwXKm"

    assert to_string(%Polyjuice.Util.URI{
             type: :user,
             identifier: "@hubert:uhoreg.ca"
           }) == "https://matrix.to/#/%40hubert%3Auhoreg.ca"
  end
end
