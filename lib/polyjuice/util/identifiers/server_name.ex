# Copyright 2022 Hubert Chathi <hubert@uhoreg.ca>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-FileCopyrightText: 2021 Nicolas Jouanin <nico@beerfactory.org>
# SPDX-License-Identifier: Apache-2.0

defmodule Polyjuice.Util.Identifiers.V1.ServerName do
  @moduledoc """
  Parse a server name.

  Syntax for server names: https://spec.matrix.org/unstable/appendices/#server-name
  """

  @type t :: %__MODULE__{
          fullname: String.t(),
          hostname: String.t(),
          port: integer | nil
        }

  @enforce_keys [:fullname, :hostname]

  defstruct [
    :fullname,
    :hostname,
    :port
  ]

  @behaviour Polyjuice.Util.Identifiers

  @impl Polyjuice.Util.Identifiers
  def new!(name) do
    case parse(name) do
      {:ok, server_name} -> server_name
      {:error, error} -> raise ArgumentError, to_string(error)
    end
  end

  @impl Polyjuice.Util.Identifiers
  def new(name), do: parse(name)

  @impl Polyjuice.Util.Identifiers
  def valid?(name) do
    case parse(name) do
      {:ok, _} -> true
      _ -> false
    end
  end

  def fqid(%__MODULE__{fullname: fullname}), do: fullname

  @impl Polyjuice.Util.Identifiers
  def parse(id) when is_binary(id) do
    case Regex.run(
           ~r/^((\d{1,3}\.\d{1,3}\.\d{1,3}.\d{1,3})|([-.0-9a-zA-Z]{1,255})|\[([:.0-9a-fA-F]{2,45})\])(?::(\d{1,5}))?$/,
           id
         ) do
      [_, hostname, ipv4, "", "", port] when ipv4 != "" ->
        case String.to_charlist(ipv4) |> :inet.parse_ipv4_address() do
          {:ok, _} ->
            {:ok, %__MODULE__{fullname: id, hostname: hostname, port: String.to_integer(port)}}

          {:error, _} ->
            {:error, :invalid_server_name}
        end

      [_, hostname, "", hostname, "", port] when hostname != "" ->
        {:ok, %__MODULE__{fullname: id, hostname: hostname, port: String.to_integer(port)}}

      [_, hostname, "", "", ipv6, port] ->
        case String.to_charlist(ipv6) |> :inet.parse_ipv6_address() do
          {:ok, _} ->
            {:ok, %__MODULE__{fullname: id, hostname: hostname, port: String.to_integer(port)}}

          {:error, _} ->
            {:error, :invalid_server_name}
        end

      [_, hostname, ipv4] when ipv4 != "" ->
        case String.to_charlist(ipv4) |> :inet.parse_ipv4_address() do
          {:ok, _} -> {:ok, %__MODULE__{fullname: id, hostname: hostname, port: nil}}
          {:error, _} -> {:error, :invalid_server_name}
        end

      [_, hostname, "", hostname] when hostname != "" ->
        {:ok, %__MODULE__{fullname: id, hostname: hostname, port: nil}}

      [_, hostname, "", "", ipv6] ->
        case String.to_charlist(ipv6) |> :inet.parse_ipv6_address() do
          {:ok, _} -> {:ok, %__MODULE__{fullname: id, hostname: hostname, port: nil}}
          {:error, _} -> {:error, :invalid_server_name}
        end

      _ ->
        {:error, :invalid_server_name}
    end
  end
end

defimpl String.Chars, for: Polyjuice.Util.Identifiers.V1.ServerName do
  def to_string(%{fullname: fullname}), do: fullname
end

defimpl Jason.Encoder, for: Polyjuice.Util.Identifiers.V1.ServerName do
  def encode(this, opts) do
    Polyjuice.Util.Identifiers.V1.ServerName.fqid(this) |> Jason.Encode.string(opts)
  end
end
