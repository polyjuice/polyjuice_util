# Copyright 2021 Hubert Chathi <hubert@uhoreg.ca>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-FileCopyrightText: 2021 Hubert Chathi <hubert@uhoreg.ca>
# SPDX-License-Identifier: Apache-2.0

defmodule Polyjuice.Util.RoomVersionTest do
  use ExUnit.Case
  doctest Polyjuice.Util.RoomVersion

  test "redaction" do
    assert Polyjuice.Util.RoomVersion.redact(
             "2",
             %{
               "content" => %{
                 "body" => "Here is the message content"
               },
               "event_id" => "$0:domain",
               "origin" => "domain",
               "origin_server_ts" => 1_000_000,
               "type" => "m.room.message",
               "room_id" => "!r:domain",
               "sender" => "@u:domain",
               "signatures" => %{},
               "unsigned" => %{
                 "age_ts" => 1_000_000
               }
             }
           ) == {
             :ok,
             %{
               "content" => %{},
               "event_id" => "$0:domain",
               "origin" => "domain",
               "origin_server_ts" => 1_000_000,
               "type" => "m.room.message",
               "room_id" => "!r:domain",
               "sender" => "@u:domain",
               "signatures" => %{}
             }
           }

    assert Polyjuice.Util.RoomVersion.redact(
             "3",
             %{
               "content" => %{
                 "body" => "Here is the message content"
               },
               "event_id" => "$0:domain",
               "origin" => "domain",
               "origin_server_ts" => 1_000_000,
               "type" => "m.room.message",
               "room_id" => "!r:domain",
               "sender" => "@u:domain",
               "signatures" => %{},
               "unsigned" => %{
                 "age_ts" => 1_000_000
               }
             }
           ) == {
             :ok,
             %{
               "content" => %{},
               "event_id" => "$0:domain",
               "origin" => "domain",
               "origin_server_ts" => 1_000_000,
               "type" => "m.room.message",
               "room_id" => "!r:domain",
               "sender" => "@u:domain",
               "signatures" => %{}
             }
           }

    assert Polyjuice.Util.RoomVersion.redact(
             "4",
             %{
               "content" => %{
                 "body" => "Here is the message content"
               },
               "event_id" => "$0:domain",
               "origin" => "domain",
               "origin_server_ts" => 1_000_000,
               "type" => "m.room.message",
               "room_id" => "!r:domain",
               "sender" => "@u:domain",
               "signatures" => %{},
               "unsigned" => %{
                 "age_ts" => 1_000_000
               }
             }
           ) == {
             :ok,
             %{
               "content" => %{},
               "event_id" => "$0:domain",
               "origin" => "domain",
               "origin_server_ts" => 1_000_000,
               "type" => "m.room.message",
               "room_id" => "!r:domain",
               "sender" => "@u:domain",
               "signatures" => %{}
             }
           }

    assert Polyjuice.Util.RoomVersion.redact(
             "5",
             %{
               "content" => %{
                 "body" => "Here is the message content"
               },
               "event_id" => "$0:domain",
               "origin" => "domain",
               "origin_server_ts" => 1_000_000,
               "type" => "m.room.message",
               "room_id" => "!r:domain",
               "sender" => "@u:domain",
               "signatures" => %{},
               "unsigned" => %{
                 "age_ts" => 1_000_000
               }
             }
           ) == {
             :ok,
             %{
               "content" => %{},
               "event_id" => "$0:domain",
               "origin" => "domain",
               "origin_server_ts" => 1_000_000,
               "type" => "m.room.message",
               "room_id" => "!r:domain",
               "sender" => "@u:domain",
               "signatures" => %{}
             }
           }

    assert Polyjuice.Util.RoomVersion.redact(
             "1",
             %{
               "content" => %{
                 "body" => "Here is the message content",
                 "membership" => "join"
               },
               "state_key" => "@user:example.com",
               "event_id" => "$0:domain",
               "origin" => "domain",
               "origin_server_ts" => 1_000_000,
               "type" => "m.room.member",
               "room_id" => "!r:domain",
               "sender" => "@u:domain",
               "signatures" => %{},
               "unsigned" => %{
                 "age_ts" => 1_000_000
               }
             }
           ) == {
             :ok,
             %{
               "content" => %{
                 "membership" => "join"
               },
               "state_key" => "@user:example.com",
               "event_id" => "$0:domain",
               "origin" => "domain",
               "origin_server_ts" => 1_000_000,
               "type" => "m.room.member",
               "room_id" => "!r:domain",
               "sender" => "@u:domain",
               "signatures" => %{}
             }
           }

    assert Polyjuice.Util.RoomVersion.redact(
             "1",
             %{
               "content" => %{
                 "body" => "Here is the message content",
                 "creator" => "@user:example.com"
               },
               "state_key" => "",
               "event_id" => "$0:domain",
               "origin" => "domain",
               "origin_server_ts" => 1_000_000,
               "type" => "m.room.create",
               "room_id" => "!r:domain",
               "sender" => "@u:domain",
               "signatures" => %{},
               "unsigned" => %{
                 "age_ts" => 1_000_000
               }
             }
           ) == {
             :ok,
             %{
               "content" => %{
                 "creator" => "@user:example.com"
               },
               "state_key" => "",
               "event_id" => "$0:domain",
               "origin" => "domain",
               "origin_server_ts" => 1_000_000,
               "type" => "m.room.create",
               "room_id" => "!r:domain",
               "sender" => "@u:domain",
               "signatures" => %{}
             }
           }

    assert Polyjuice.Util.RoomVersion.redact(
             "1",
             %{
               "content" => %{
                 "body" => "Here is the message content",
                 "join_rule" => "public"
               },
               "state_key" => "",
               "event_id" => "$0:domain",
               "origin" => "domain",
               "origin_server_ts" => 1_000_000,
               "type" => "m.room.join_rules",
               "room_id" => "!r:domain",
               "sender" => "@u:domain",
               "signatures" => %{},
               "unsigned" => %{
                 "age_ts" => 1_000_000
               }
             }
           ) == {
             :ok,
             %{
               "content" => %{
                 "join_rule" => "public"
               },
               "state_key" => "",
               "event_id" => "$0:domain",
               "origin" => "domain",
               "origin_server_ts" => 1_000_000,
               "type" => "m.room.join_rules",
               "room_id" => "!r:domain",
               "sender" => "@u:domain",
               "signatures" => %{}
             }
           }

    assert Polyjuice.Util.RoomVersion.redact(
             "1",
             %{
               "content" => %{
                 "body" => "Here is the message content",
                 "ban" => 50,
                 "events" => %{
                   "m.room.name" => 100,
                   "m.room.power_levels" => 100
                 },
                 "events_default" => 0,
                 "invite" => 50,
                 "kick" => 50,
                 "notifications" => %{
                   "room" => 20
                 },
                 "redact" => 50,
                 "state_default" => 50,
                 "users" => %{
                   "@example:localhost" => 100
                 },
                 "users_default" => 0
               },
               "state_key" => "",
               "event_id" => "$0:domain",
               "origin" => "domain",
               "origin_server_ts" => 1_000_000,
               "type" => "m.room.power_levels",
               "room_id" => "!r:domain",
               "sender" => "@u:domain",
               "signatures" => %{},
               "unsigned" => %{
                 "age_ts" => 1_000_000
               }
             }
           ) == {
             :ok,
             %{
               "content" => %{
                 "ban" => 50,
                 "events" => %{
                   "m.room.name" => 100,
                   "m.room.power_levels" => 100
                 },
                 "events_default" => 0,
                 "kick" => 50,
                 "redact" => 50,
                 "state_default" => 50,
                 "users" => %{
                   "@example:localhost" => 100
                 },
                 "users_default" => 0
               },
               "state_key" => "",
               "event_id" => "$0:domain",
               "origin" => "domain",
               "origin_server_ts" => 1_000_000,
               "type" => "m.room.power_levels",
               "room_id" => "!r:domain",
               "sender" => "@u:domain",
               "signatures" => %{}
             }
           }

    assert Polyjuice.Util.RoomVersion.redact(
             "1",
             %{
               "content" => %{
                 "body" => "Here is the message content",
                 "aliases" => ["#aroom:example.com", "#anothername:example.com"]
               },
               "state_key" => "example.com",
               "event_id" => "$0:domain",
               "origin" => "domain",
               "origin_server_ts" => 1_000_000,
               "type" => "m.room.aliases",
               "room_id" => "!r:domain",
               "sender" => "@u:domain",
               "signatures" => %{},
               "unsigned" => %{
                 "age_ts" => 1_000_000
               }
             }
           ) == {
             :ok,
             %{
               "content" => %{
                 "aliases" => ["#aroom:example.com", "#anothername:example.com"]
               },
               "state_key" => "example.com",
               "event_id" => "$0:domain",
               "origin" => "domain",
               "origin_server_ts" => 1_000_000,
               "type" => "m.room.aliases",
               "room_id" => "!r:domain",
               "sender" => "@u:domain",
               "signatures" => %{}
             }
           }

    assert Polyjuice.Util.RoomVersion.redact(
             "1",
             %{
               "content" => %{
                 "body" => "Here is the message content",
                 "history_visibility" => "invited"
               },
               "state_key" => "",
               "event_id" => "$0:domain",
               "origin" => "domain",
               "origin_server_ts" => 1_000_000,
               "type" => "m.room.history_visibility",
               "room_id" => "!r:domain",
               "sender" => "@u:domain",
               "signatures" => %{},
               "unsigned" => %{
                 "age_ts" => 1_000_000
               }
             }
           ) == {
             :ok,
             %{
               "content" => %{
                 "history_visibility" => "invited"
               },
               "state_key" => "",
               "event_id" => "$0:domain",
               "origin" => "domain",
               "origin_server_ts" => 1_000_000,
               "type" => "m.room.history_visibility",
               "room_id" => "!r:domain",
               "sender" => "@u:domain",
               "signatures" => %{}
             }
           }

    assert Polyjuice.Util.RoomVersion.redact(
             "1",
             %{
               "content" => %{
                 "body" => "Here is the message content"
               },
               "state_key" => "example.com",
               "event_id" => "$0:domain",
               "origin" => "domain",
               "origin_server_ts" => 1_000_000,
               "room_id" => "!r:domain",
               "sender" => "@u:domain",
               "signatures" => %{},
               "unsigned" => %{
                 "age_ts" => 1_000_000
               }
             }
           ) == {
             :ok,
             %{
               "content" => %{},
               "state_key" => "example.com",
               "event_id" => "$0:domain",
               "origin" => "domain",
               "origin_server_ts" => 1_000_000,
               "room_id" => "!r:domain",
               "sender" => "@u:domain",
               "signatures" => %{}
             }
           }

    assert Polyjuice.Util.RoomVersion.redact(
             "1",
             %{
               "event_id" => "$0:domain",
               "origin" => "domain",
               "origin_server_ts" => 1_000_000,
               "room_id" => "!r:domain",
               "sender" => "@u:domain",
               "signatures" => %{},
               "unsigned" => %{
                 "age_ts" => 1_000_000
               }
             }
           ) == {
             :ok,
             %{
               "event_id" => "$0:domain",
               "origin" => "domain",
               "origin_server_ts" => 1_000_000,
               "room_id" => "!r:domain",
               "sender" => "@u:domain",
               "signatures" => %{}
             }
           }
  end

  test "compute_content_hash" do
    {:ok, hash1} =
      Polyjuice.Util.RoomVersion.compute_content_hash(
        "1",
        %{
          "event_id" => "$0:domain",
          "origin" => "domain",
          "origin_server_ts" => 1_000_000,
          "room_id" => "!r:domain",
          "sender" => "@u:domain",
          "hashes" => %{},
          "signatures" => %{},
          "unsigned" => %{
            "age_ts" => 1_000_000
          }
        }
      )

    {:ok, hash2} =
      Polyjuice.Util.RoomVersion.compute_content_hash(
        "1",
        %{
          "event_id" => "$0:domain",
          "origin" => "domain",
          "origin_server_ts" => 1_000_000,
          "room_id" => "!r:domain",
          "sender" => "@u:domain"
        }
      )

    assert hash1 == hash2
  end

  describe "authorized? room v1" do
    test "allows m.room.create events only when fields are valid" do
      assert Polyjuice.Util.RoomVersion.authorized?(
               "1",
               %{
                 "type" => "m.room.create",
                 "sender" => "@alice:example.org",
                 "prev_events" => [1],
                 "room_id" => "!abc:example.org",
                 "content" => %{"creator" => "@alice:example.org"}
               },
               %{}
             ) == false

      assert Polyjuice.Util.RoomVersion.authorized?(
               "1",
               %{
                 "type" => "m.room.create",
                 "sender" => "@alice:example.org",
                 "prev_events" => [],
                 "room_id" => "!abc:not-example.org",
                 "content" => %{"creator" => "@alice:example.org"}
               },
               %{}
             ) == false

      assert Polyjuice.Util.RoomVersion.authorized?(
               "1",
               %{
                 "type" => "m.room.create",
                 "sender" => "@alice:example.org",
                 "prev_events" => [],
                 "room_id" => "!abc:example.org",
                 "content" => %{}
               },
               %{}
             ) == false

      assert Polyjuice.Util.RoomVersion.authorized?(
               "1",
               %{
                 "type" => "m.room.create",
                 "sender" => "@alice:example.org",
                 "prev_events" => [],
                 "room_id" => "!abc:example.org",
                 "content" => %{"creator" => "@alice:example.org"}
               },
               %{}
             ) == true
    end

    test "allows an m.room.message from a joined user" do
      user_id = "@someone:example.org"
      room_id = "!abc:example.org"
      create_event_id = "$abcdef:example.org"
      membership_event_id = "$zbcdef:example.org"

      state = %{
        {"m.room.create", ""} => %{
          "type" => "m.room.create",
          "sender" => user_id,
          "state_key" => "",
          "prev_events" => [],
          "room_id" => room_id,
          "content" => %{"creator" => user_id},
          "event_id" => create_event_id
        },
        {"m.room.member", user_id} => %{
          "type" => "m.room.member",
          "sender" => user_id,
          "state_key" => user_id,
          "prev_events" => [create_event_id],
          "room_id" => room_id,
          "content" => %{"membership" => "join"},
          "event_id" => membership_event_id
        }
      }

      refute Polyjuice.Util.RoomVersion.authorized?(
               "1",
               %{
                 "type" => "m.room.message",
                 "sender" => "@some-random-user:example.org",
                 "prev_events" => [membership_event_id],
                 "room_id" => room_id,
                 "content" => %{"msgtype" => "m.text", "body" => "hello world"}
               },
               state,
               [Map.fetch!(state, {"m.room.create", ""})]
             )

      assert Polyjuice.Util.RoomVersion.authorized?(
               "1",
               %{
                 "type" => "m.room.message",
                 "sender" => user_id,
                 "prev_events" => [membership_event_id],
                 "room_id" => room_id,
                 "content" => %{"msgtype" => "m.text", "body" => "hello world"}
               },
               state,
               Map.values(state)
             )
    end

    test "allows an m.room.power_levels only when fields are valid" do
      user_id = "@someone:example.org"
      room_id = "!abc:example.org"
      create_event_id = "$abcdef:example.org"
      membership_event_id = "$zbcdef:example.org"

      state = %{
        {"m.room.create", ""} => %{
          "type" => "m.room.create",
          "sender" => user_id,
          "state_key" => "",
          "prev_events" => [],
          "room_id" => room_id,
          "content" => %{"creator" => user_id},
          "event_id" => create_event_id
        },
        {"m.room.member", user_id} => %{
          "type" => "m.room.member",
          "sender" => user_id,
          "state_key" => user_id,
          "prev_events" => [create_event_id],
          "room_id" => room_id,
          "content" => %{"membership" => "join"},
          "event_id" => membership_event_id
        }
      }

      refute Polyjuice.Util.RoomVersion.authorized?(
               "1",
               %{
                 "type" => "m.room.power_levels",
                 "sender" => user_id,
                 "state_key" => "",
                 "room_id" => room_id,
                 "content" => %{"users" => %{"blahblah not a user ID" => 80}}
               },
               state,
               Map.values(state)
             )

      refute Polyjuice.Util.RoomVersion.authorized?(
               "1",
               %{
                 "type" => "m.room.power_levels",
                 "sender" => user_id,
                 "state_key" => "",
                 "room_id" => room_id,
                 "content" => %{"users" => %{user_id => "abc"}}
               },
               state,
               Map.values(state)
             )

      assert Polyjuice.Util.RoomVersion.authorized?(
               "1",
               %{
                 "type" => "m.room.power_levels",
                 "sender" => user_id,
                 "state_key" => "",
                 "room_id" => room_id,
                 "content" => %{"users" => %{user_id => 70}}
               },
               state,
               Map.values(state)
             )
    end

    test "rejects events from other homeservers when room has federate: false" do
      user_id = "@someone:example.org"
      room_id = "!abc:example.org"

      state = %{
        {"m.room.create", ""} => %{
          "type" => "m.room.create",
          "sender" => user_id,
          "state_key" => "",
          "room_id" => room_id,
          "content" => %{"creator" => user_id, "m.federate" => false},
          "event_id" => "$create_event"
        },
        {"m.room.member", user_id} => %{
          "type" => "m.room.member",
          "sender" => user_id,
          "state_key" => user_id,
          "room_id" => room_id,
          "content" => %{"membership" => "join"},
          "event_id" => "$membership_event"
        },
        {"m.room.join_rules", ""} => %{
          "type" => "m.room.join_rules",
          "sender" => user_id,
          "state_key" => "",
          "room_id" => room_id,
          "content" => %{"join_rule" => "public"},
          "event_id" => "$join_rule_event"
        }
      }

      # an event from our own homeserver should be accepted
      assert Polyjuice.Util.RoomVersion.authorized?(
               "1",
               %{
                 "type" => "m.room.member",
                 "sender" => "@someone_else:example.org",
                 "state_key" => "@someone_else:example.org",
                 "room_id" => room_id,
                 "content" => %{"membership" => "join"}
               },
               state,
               Map.values(state)
             )

      # an event from another homeserver should be rejected
      refute Polyjuice.Util.RoomVersion.authorized?(
               "1",
               %{
                 "type" => "m.room.member",
                 "sender" => "@someone:example.com",
                 "state_key" => "@someone:example.com",
                 "room_id" => room_id,
                 "content" => %{"membership" => "join"}
               },
               state,
               Map.values(state)
             )
    end
  end

  describe "get_default_pl/2" do
    test "returns a room member's powerlevel set in the state" do
      user_id = "@someone:some-server.org"
      state = %{{"m.room.power_levels", ""} => %{"content" => %{"users" => %{user_id => 42}}}}

      assert {:ok, 42} = Polyjuice.Util.RoomVersion.get_user_pl("1", user_id, state)
    end

    test "returns the default user powerlevel if the member doesn't have an explicit powerlevel set" do
      user_id = "@someone:some-server.org"
      state = %{{"m.room.power_levels", ""} => %{"content" => %{"users_default" => 30}}}

      assert {:ok, 30} = Polyjuice.Util.RoomVersion.get_user_pl("1", user_id, state)
    end

    test "returns a powerlevel of 100 if the user is the room creator, when there isn't a m.room.power_levels in the state" do
      user_id = "@someone:example.org"

      state = %{
        {"m.room.create", ""} => %{
          "type" => "m.room.create",
          "sender" => user_id,
          "prev_events" => [],
          "room_id" => "!abc:example.org",
          "content" => %{"creator" => user_id}
        }
      }

      assert {:ok, 100} = Polyjuice.Util.RoomVersion.get_user_pl("1", user_id, state)
    end
  end
end
