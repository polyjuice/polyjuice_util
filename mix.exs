defmodule PolyjuiceUtil.MixProject do
  use Mix.Project

  def project do
    [
      app: :polyjuice_util,
      description: "Utility functions for the Matrix.org communications protocol",
      version: "0.2.2",
      elixir: "~> 1.7",
      start_permanent: Mix.env() == :prod,
      deps: deps(),

      # Docs
      name: "Polyjuice Util",
      source_url: "https://gitlab.com/polyjuice/polyjuice_util",
      homepage_url: "https://www.uhoreg.ca/programming/matrix/polyjuice",
      docs: [
        # The main page in the docs
        main: "readme",
        # logo: "path/to/logo.png",
        extras: ["README.md"],
        groups_for_modules: [
          Errors: [
            ~r/Polyjuice\.Util\.ClientAPIErrors.*/,
            ~r/Polyjuice\.Util\.ClientErrors.*/
          ]
        ]
      ],
      package: [
        maintainers: ["Hubert Chathi"],
        licenses: ["Apache-2.0"],
        links: %{
          "Source" => "https://gitlab.com/polyjuice/polyjuice_util"
        }
      ],

      # Tests
      test_coverage: [
        summary: [threshold: 0],
        ignore_modules: [
          ~r/Jason\.Encoder\.Polyjuice\.Util\.ClientAPIErrors\..*/,
          ~r/Polyjuice\.Util\.ClientAPIErrors\..*/
        ]
      ],
      consolidate_protocols: Mix.env() != :test
    ]
  end

  def application do
    [
      extra_applications: [:logger, :crypto]
    ]
  end

  defp deps do
    uuid =
      if System.version() |> Version.match?("~> 1.13"),
        do: {:uniq, "~> 0.6"},
        else: {:uuid, "~> 1.1"}

    [
      {:dialyxir, "~> 1.0", only: :dev, runtime: false},
      {:ex_doc, "~> 0.21", only: :dev, runtime: false},
      uuid,
      {:jason, "~> 1.4"},
      {:benchee, "~> 1.0", only: :dev}
    ]
  end
end

# SPDX-FileCopyrightText: 2019-2021 Hubert Chathi <hubert@uhoreg.ca>
# SPDX-License-Identifier: Apache-2.0
