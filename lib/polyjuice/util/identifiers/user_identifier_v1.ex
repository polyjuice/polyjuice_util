# Copyright 2021 Nicolas Jouanin <nico@beerfactory.org>
# Copyright 2022 Hubert Chathi <hubert@uhoreg.ca>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-FileCopyrightText: 2021 Nicolas Jouanin <nico@beerfactory.org>
# SPDX-FileCopyrightText: 2022 Hubert Chathi <hubert@uhoreg.ca>
# SPDX-License-Identifier: Apache-2.0

defmodule Polyjuice.Util.Identifiers.V1.UserIdentifier do
  @type t :: %__MODULE__{
          sigil: String.t(),
          localpart: String.t(),
          domain: String.t()
        }
  alias Polyjuice.Util.Identifiers.V1.UserIdentifier
  alias Polyjuice.Util.Identifiers.V1.ServerName

  @enforce_keys [:sigil, :localpart, :domain]

  defstruct [
    :sigil,
    :localpart,
    :domain
  ]

  @sigil "@"
  @localpart_regex ~r|^[a-z0-9\-\.\=\_\/]+$|
  @max_length 255

  @behaviour Polyjuice.Util.Identifiers

  @randomize_length 15

  @spec generate(domain :: String.t()) :: UserIdentifier.t()
  def generate(domain) do
    {:ok, user} = generate_localpart() |> Polyjuice.Util.Identifiers.V1.UserIdentifier.new(domain)
    user
  end

  @doc """
  Generate a unique user localpoart
  """
  @spec generate_localpart() :: String.t()
  def generate_localpart() do
    Polyjuice.Util.Randomizer.randomize(@randomize_length, :downcase_numeric)
  end

  @impl Polyjuice.Util.Identifiers
  def new!(identifier) do
    case parse(identifier) do
      {:ok, user} -> user
      {:error, error} -> raise ArgumentError, to_string(error)
    end
  end

  @impl Polyjuice.Util.Identifiers
  def new([localpart, domain]), do: new(localpart, domain)

  @impl Polyjuice.Util.Identifiers
  def new({localpart, domain}), do: new(localpart, domain)

  @impl Polyjuice.Util.Identifiers
  def new(%{localpart: localpart, domain: domain}), do: new(localpart, domain)

  @impl Polyjuice.Util.Identifiers
  def new(identifier), do: parse(identifier)

  @spec new(String.t(), String.t()) :: {:ok, UserIdentifier.t()} | {:error, atom}
  def new(localpart, domain) when is_binary(localpart) and is_binary(domain) do
    user = %UserIdentifier{sigil: @sigil, localpart: localpart, domain: domain}

    if valid?(user) do
      {:ok, user}
    else
      {:error, :invalid_user_id}
    end
  end

  @impl Polyjuice.Util.Identifiers
  def valid?(%UserIdentifier{} = this) do
    Regex.match?(@localpart_regex, this.localpart) && String.length(fqid(this)) < @max_length &&
      ServerName.valid?(this.domain) && String.length(this.localpart) > 0
  end

  @impl Polyjuice.Util.Identifiers
  def valid?(identifier_string) do
    case parse(identifier_string) do
      {:ok, _} -> true
      _ -> false
    end
  end

  def valid_localpart?(localpart) when is_binary(localpart) do
    Regex.match?(@localpart_regex, localpart) and String.length(localpart) > 0
  end

  @doc """
  Map a localpart with possibly invalid characters into the valid allowed characters.

  Uses the method described at
  https://spec.matrix.org/unstable/appendices/#mapping-from-other-character-sets
  """
  def map_localpart(localpart) when is_binary(localpart) do
    localpart
    |> :binary.bin_to_list()
    |> Enum.map(fn
      c when c >= ?0 and c <= ?9 -> c
      c when c >= ?a and c <= ?z -> c
      ?. -> ?.
      ?_ -> [?_, ?_]
      ?- -> ?-
      ?/ -> ?/
      c when c >= ?A and c <= ?Z -> [?_, c + 0x20]
      c -> [?=, Integer.to_string(c, 16) |> String.downcase()]
    end)
    |> IO.iodata_to_binary()
  end

  def fqid(%UserIdentifier{} = this) do
    "#{@sigil}#{this.localpart}:#{this.domain}"
  end

  @impl Polyjuice.Util.Identifiers
  @spec parse(String.t()) :: {:ok, UserIdentifier.t()} | {:error, atom}
  def parse(id) when is_binary(id) do
    with @sigil <> id <- id,
         [localpart, domain] <- String.split(id, ":", parts: 2, trim: true) do
      new(localpart, domain)
    else
      _ -> {:error, :invalid_user_id}
    end
  end
end

defimpl String.Chars, for: Polyjuice.Util.Identifiers.V1.UserIdentifier do
  def to_string(this), do: Polyjuice.Util.Identifiers.V1.UserIdentifier.fqid(this)
end

defimpl Jason.Encoder, for: Polyjuice.Util.Identifiers.V1.UserIdentifier do
  def encode(this, opts) do
    Polyjuice.Util.Identifiers.V1.UserIdentifier.fqid(this) |> Jason.Encode.string(opts)
  end
end
