# Copyright 2021 Nicolas Jouanin <nico@beerfactory.org>
# Copyright 2022 Hubert Chathi <hubert@uhoreg.ca>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-FileCopyrightText: 2021 Nicolas Jouanin <nico@beerfactory.org>
# SPDX-FileCopyrightText: 2022 Hubert Chathi <hubert@uhoreg.ca>
# SPDX-License-Identifier: Apache-2.0

defmodule Polyjuice.Util.IdentfierTest do
  use ExUnit.Case
  doctest Polyjuice.Util.Identifiers

  alias Polyjuice.Util.Identifiers
  alias Polyjuice.Util.Identifiers.V1.UserIdentifier
  alias Polyjuice.Util.Identifiers.V1.RoomIdentifier
  alias Polyjuice.Util.Identifiers.V1.EventIdentifier
  alias Polyjuice.Util.Identifiers.V1.RoomAliasIdentifier
  alias Polyjuice.Util.Identifiers.V1.ServerName
  doctest Polyjuice.Util.Identifiers

  test "Create new user identifier" do
    {:ok, id} = UserIdentifier.new("localpart", "domain")
    assert %UserIdentifier{sigil: "@", localpart: "localpart", domain: "domain"} == id
    assert "#{id}" == "@localpart:domain"
  end

  test "Create V0 user identifier" do
    alias Polyjuice.Util.Identifiers.V0.UserIdentifier
    {:ok, id} = UserIdentifier.new("!oca!@", "domain")
    assert %UserIdentifier{sigil: "@", localpart: "!oca!@", domain: "domain"} == id
    assert "#{id}" == "@!oca!@:domain"
  end

  test "Create new room identifier" do
    {:ok, id} = RoomIdentifier.new("opaque_id", "domain")
    assert %RoomIdentifier{sigil: "!", opaque_id: "opaque_id", domain: "domain"} == id
    assert "#{id}" == "!opaque_id:domain"
  end

  test "Create new event V1 identifier" do
    {:ok, id} = EventIdentifier.new("opaque_id", "domain")
    assert %EventIdentifier{sigil: "$", opaque_id: "opaque_id", domain: "domain"} == id
    assert "#{id}" == "$opaque_id:domain"
  end

  test "Create new event V3 identifier" do
    {:ok, id} = Identifiers.V3.EventIdentifier.new("$AgfxdDHkZnJwKGlwMtrqYZ+pPz1baK/15fPjycphmGg")

    assert %Identifiers.V3.EventIdentifier{
             sigil: "$",
             opaque_id: "AgfxdDHkZnJwKGlwMtrqYZ+pPz1baK/15fPjycphmGg"
           } == id

    assert "#{id}" == "$AgfxdDHkZnJwKGlwMtrqYZ+pPz1baK/15fPjycphmGg"
  end

  test "Create new event V4 identifier" do
    {:ok, id} = Identifiers.V4.EventIdentifier.new("$AgfxdDHkZnJwKGlwMtrqYZ-pPz1baK_15fPjycphmGg")

    assert %Identifiers.V4.EventIdentifier{
             sigil: "$",
             opaque_id: "AgfxdDHkZnJwKGlwMtrqYZ-pPz1baK_15fPjycphmGg"
           } == id

    assert "#{id}" == "$AgfxdDHkZnJwKGlwMtrqYZ-pPz1baK_15fPjycphmGg"
  end

  test "Create new room alias identifier" do
    {:ok, id} = RoomAliasIdentifier.new("opaque_id", "domain")
    assert %RoomAliasIdentifier{sigil: "#", opaque_id: "opaque_id", domain: "domain"} == id
    assert "#{id}" == "#opaque_id:domain"
  end

  test "Generate user identifier" do
    id = UserIdentifier.generate("domain")
    %UserIdentifier{localpart: localpart, domain: "domain"} = id
    assert localpart != ""
  end

  test "Fail create invalid user identifier" do
    assert {:error, :invalid_user_id} == UserIdentifier.new("INVALID", "domain")
    assert {:error, :invalid_user_id} == UserIdentifier.new("user", "n*t a valid domain")
  end

  test "Valid user identifier" do
    assert UserIdentifier.valid?("@localpart:domain")
  end

  test "Invalid user identifier" do
    assert !UserIdentifier.valid?("@LOCALPART:domain")
  end

  test "User ID localpart checking" do
    assert UserIdentifier.valid_localpart?("localpart")
    assert not UserIdentifier.valid_localpart?("LOCALPART")
    assert Identifiers.V0.UserIdentifier.valid_localpart?("LOCALPART")
    assert not Identifiers.V0.UserIdentifier.valid_localpart?("LOCAL:PART")
  end

  test "User ID localpart mapping" do
    assert UserIdentifier.map_localpart("abc.ABC_!@#=á") == "abc._a_b_c__=21=40=23=3d=c3=a1"
  end

  test "Generate room identifier" do
    id = RoomIdentifier.generate("domain")
    %RoomIdentifier{opaque_id: localpart, domain: "domain"} = id
    assert localpart != ""
  end

  test "ServerName parsing" do
    assert ServerName.valid?("uhoreg.ca")
    assert ServerName.valid?("localhost:8008")
    assert ServerName.valid?("127.0.0.1")
    assert ServerName.valid?("127.0.0.1:8008")
    assert ServerName.valid?("[::1]")
    assert ServerName.valid?("[::1]:8008")
    assert not ServerName.valid?("127.0.0.256")
    assert not ServerName.valid?("127.0.0.256:8008")
    assert not ServerName.valid?("[ab]")
    assert not ServerName.valid?("[ab]:8008")
    assert not ServerName.valid?("!:8008")

    assert {:ok, %ServerName{fullname: "uhoreg.ca", hostname: "uhoreg.ca", port: nil}} ==
             ServerName.new("uhoreg.ca")

    assert {:ok, %ServerName{fullname: "uhoreg.ca:08008", hostname: "uhoreg.ca", port: 8008}} ==
             ServerName.new("uhoreg.ca:08008")

    assert "#{ServerName.new!("uhoreg.ca")}" == "uhoreg.ca"
    assert "#{ServerName.new!("uhoreg.ca:0123")}" == "uhoreg.ca:0123"
  end

  test "JSON encoding" do
    assert Jason.encode!(%{
             event_id_v1: Identifiers.V1.EventIdentifier.new!("$event_id:example.org"),
             event_id_v3:
               Identifiers.V3.EventIdentifier.new!("$AgfxdDHkZnJwKGlwMtrqYZ+pPz1baK/15fPjycphmGg"),
             event_id_v4:
               Identifiers.V4.EventIdentifier.new!("$AgfxdDHkZnJwKGlwMtrqYZ_pPz1baK-15fPjycphmGg"),
             room_alias: Identifiers.V1.RoomAliasIdentifier.new!("#alias:example.org"),
             room_id: Identifiers.V1.RoomIdentifier.new!("!room_id:example.org"),
             server_name: Identifiers.V1.ServerName.new!("example.org"),
             user_id_v0: Identifiers.V0.UserIdentifier.new!("@User:example.org"),
             user_id_v1: Identifiers.V1.UserIdentifier.new!("@user:example.org")
           })
           |> Jason.decode() ==
             {:ok,
              %{
                "event_id_v1" => "$event_id:example.org",
                "event_id_v3" => "$AgfxdDHkZnJwKGlwMtrqYZ+pPz1baK/15fPjycphmGg",
                "event_id_v4" => "$AgfxdDHkZnJwKGlwMtrqYZ_pPz1baK-15fPjycphmGg",
                "room_alias" => "#alias:example.org",
                "room_id" => "!room_id:example.org",
                "server_name" => "example.org",
                "user_id_v0" => "@User:example.org",
                "user_id_v1" => "@user:example.org"
              }}
  end
end
