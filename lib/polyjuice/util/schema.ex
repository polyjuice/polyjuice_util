# Copyright 2023 Hubert Chathi <hubert@uhoreg.ca>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

defmodule Polyjuice.Util.Schema do
  @moduledoc """
  Validates and parses a JSON value according to a schema.
  """

  @doc ~S"""
  Check that a JSON value matches the specified pattern and returns a parsed
  version.

  Patterns can be:

  - a map: the JSON value must be a JSON object that has the keys given in the
    pattern.  The values in the pattern are themselves patterns that the values
    in the JSON object must match, recursively.  One exception is if a value in
    the pattern is in the form `[pattern, options...]`, which adds options to
    the pattern.  Accepted options are `:optional`, which means that the
    corresponding value in the JSON object does not have to be
    present, but if it is, it must match the pattern; and `default: v`, which
    implies the `:optional` option, but sets a default value if no value is
    given.  The `:optional` option can be given either as the atom `:optional`,
    or as `optional: true`.
  - `:any`: accepts anything
  - `:string`: accepts a string
  - `:integer`: accepts an integer
  - `:boolean`: accepts a boolean
  - a `Regex`: accepts a string that matches the regex
  - a function: If the function has arity 1, it will be called with the
    `value` as argument.  If the function has arity 2, it will be called with
    the `value` and `param` as arguments.  The function must return
    `{:ok, parsed_value}` if the value is valid, or an `:error` tuple (e.g.
    `{:error, :invalid}`) otherwise.  Some functions are provided in this module
    that can be used here, as well as helpers to create functions suitable for
    use here.

  Returns `{:ok, parsed_value}` on success, or `{:error, ...}` on failure.
  Possible error values are:

  - `{:error, :wrong_type, expected_type, value}` if the value does not match
    the expected type (e.g. if a boolean was provided when an integer was
    expected)
  - `{:error, :mismatch_regex, value}` if the value did not match the provided
    `Regex`
  - `{:error, :invalid_pattern}` if the pattern provided was invalid
  - `{:error, :invalid_value, field_path, error}` if a value in a JSON object
    (possibly nested) does not match the required pattern.  `field_path` is a
    list of strings representing the path within nested objects to reach the
    invalid value, and `error` is an `:error` tuple indicating why the value is
    invalid.
  - `{:error, :missing_value, field_path}` if a required value is missing from
    a JSON object (possibly nested)
  - any error returned by a function used as a pattern

  Examples:

      iex> Polyjuice.Util.Schema.match(
      ...>     %{"boolean" => true},
      ...>     %{"boolean" => :boolean, "optional" => [:string, :optional]}
      ...> )
      {:ok, %{"boolean" => true}}

      iex> Polyjuice.Util.Schema.match(
      ...>     %{"boolean" => true},
      ...>     %{"boolean" => :boolean, "optional" => [:string, optional: true]}
      ...> )
      {:ok, %{"boolean" => true}}

      iex> Polyjuice.Util.Schema.match(
      ...>     %{"integer" => 1},
      ...>     %{"integer" => :integer, "optional" => [:string, default: "foo"]}
      ...> )
      {:ok, %{"integer" => 1, "optional" => "foo"}}

      iex> Polyjuice.Util.Schema.match(
      ...>     %{"integer" => 1, "optional" => 0},
      ...>     %{"integer" => :integer, "optional" => [:string, default: "foo"]}
      ...> )
      {:error, :invalid_value, ["optional"], {:error, :wrong_type, :string, 0}}

  """
  @spec match(value :: any(), pattern :: any(), param :: any()) :: {:ok, any()} | :error
  def match(value, pattern, param \\ nil)

  def match(value, :any, _) do
    {:ok, value}
  end

  def match(value, :string, _) do
    if is_binary(value), do: {:ok, value}, else: {:error, :wrong_type, :string, value}
  end

  def match(value, :integer, _) do
    if is_integer(value), do: {:ok, value}, else: {:error, :wrong_type, :integer, value}
  end

  def match(value, :boolean, _) do
    if is_boolean(value), do: {:ok, value}, else: {:error, :wrong_type, :boolean, value}
  end

  def match(value, f, _) when is_function(f, 1) do
    f.(value)
  end

  def match(value, f, param) when is_function(f, 2) do
    f.(value, param)
  end

  def match(value, %Regex{} = pattern, _) do
    cond do
      not is_binary(value) -> {:error, :wrong_type, :string, value}
      Regex.match?(pattern, value) -> {:ok, value}
      true -> {:error, :mismatch_regex, pattern, value}
    end
  end

  def match(value, pattern, param) when is_map(value) and is_map(pattern) do
    Enum.reduce_while(pattern, {:ok, value}, fn {name, pat}, {:ok, acc} ->
      {optional, pat, default} =
        case pat do
          [pat | opts] ->
            Enum.reduce(opts, {false, pat, :none}, fn opt, {optional, pat, default} ->
              case opt do
                :optional ->
                  {true, pat, default}

                {:optional, optional} ->
                  if optional, do: {true, pat, default}, else: {false, pat, :none}

                {:default, default} ->
                  {true, pat, default}

                _ ->
                  {optional, pat, default}
              end
            end)

          _ ->
            {false, pat, :none}
        end

      case Map.fetch(acc, name) do
        {:ok, val} ->
          case match(val, pat, param) do
            {:ok, parsed} ->
              {:cont, {:ok, Map.put(acc, name, parsed)}}

            # handle errors that came from a recursive call to this function
            {:error, :invalid_value, field, err} ->
              {:halt, {:error, :invalid_value, [name | field], err}}

            {:error, :missing_value, field} ->
              {:halt, {:error, :missing_value, [name | field]}}

            # other errors get wrapped to indicate which field was an error
            err ->
              {:halt, {:error, :invalid_value, [name], err}}
          end

        _ ->
          cond do
            default != :none -> {:cont, {:ok, Map.put(acc, name, default)}}
            optional -> {:cont, {:ok, acc}}
            true -> {:halt, {:error, :missing_value, [name]}}
          end
      end
    end)
  end

  def match(value, pattern, _) when is_map(pattern) do
    {:error, :wrong_type, :object, value}
  end

  def match(_, _, _) do
    {:error, :invalid_pattern}
  end

  @doc """
  Function to match and parse user IDs.

  Intended to be used with `match/3`.  On success, returns `{:ok,
  parsed_value}`, where `parsed_value` is a
  `Polyjuice.Util.Identifiers.V1.UserIdentifier` (if the user ID matches the
  stricter grammar for user identifiers) or
  `Polyjuice.Util.Identifiers.V0.UserIdentifier` struct (if the user ID matches
  the historical grammar for user identifiers).  On failure, returns `{:error,
  :invalid_user_id, value}`.

  Examples:

      iex> Polyjuice.Util.Schema.match(
      ...>     "@alice:example.org",
      ...>     &Polyjuice.Util.Schema.user_id/1
      ...> )
      {:ok, Polyjuice.Util.Identifiers.V1.UserIdentifier.new!("@alice:example.org")}

      iex> Polyjuice.Util.Schema.match(
      ...>     "not_a_user_id",
      ...>     &Polyjuice.Util.Schema.user_id/1
      ...> )
      {:error, :invalid_user_id, "not_a_user_id"}

  """
  @spec user_id(value :: any()) ::
          {:ok, Polyjuice.Util.Identifiers.V1.UserIdentifer.t()}
          | {:ok, Polyjuice.Util.Identifiers.V0.UserIdentifier.t()}
          | {:error, :invalid_user_id, any()}
  def user_id(value) do
    with {:error, _} <- Polyjuice.Util.Identifiers.V1.UserIdentifier.new(value),
         {:error, _} <- Polyjuice.Util.Identifiers.V0.UserIdentifier.new(value) do
      {:error, :invalid_user_id, value}
    else
      {:ok, _} = ret -> ret
      _ -> {:error, :invalid_user_id, value}
    end
  end

  @doc """
  Function to match and parse a user ID's localpart.

  Intended to be used with `match/3`.  On success, returns `{:ok, {:v1,
  value}}` if the localpart matches the stricter grammar for localparts, or
  `{:ok, {:v0, value}}` if the localpart matches the historical grammar for
  localparts.  On failure, returns `{:error, :invalid_user_localpart, value}`.

  Examples:

      iex> Polyjuice.Util.Schema.match(
      ...>     "alice",
      ...>     &Polyjuice.Util.Schema.user_localpart/1
      ...> )
      {:ok, {:v1, "alice"}}

      iex> Polyjuice.Util.Schema.match(
      ...>     "not:valid",
      ...>     &Polyjuice.Util.Schema.user_localpart/1
      ...> )
      {:error, :invalid_user_localpart, "not:valid"}

  """
  @spec user_localpart(value :: any()) ::
          {:ok, {:v1, String.t()}}
          | {:ok, {:v0, String.t()}}
          | {:error, :invalid_user_localpart, any()}
  def user_localpart(value) do
    cond do
      Polyjuice.Util.Identifiers.V1.UserIdentifier.valid_localpart?(value) ->
        {:ok, {:v1, value}}

      Polyjuice.Util.Identifiers.V0.UserIdentifier.valid_localpart?(value) ->
        {:ok, {:v0, value}}

      true ->
        {:error, :invalid_user_localpart, value}
    end
  end

  @doc """
  Function to match and parse room IDs.

  Intended to be used with `match/3`.  On success, returns `{:ok,
  parsed_value}`, where `parsed_value` is a
  `Polyjuice.Util.Identifiers.V1.RoomIdentifier` struct.  On failure, returns
  `{:error, :invalid_room_id, value}`.

  Examples:

      iex> Polyjuice.Util.Schema.match(
      ...>     "!abc:example.org",
      ...>     &Polyjuice.Util.Schema.room_id/1
      ...> )
      {:ok, Polyjuice.Util.Identifiers.V1.RoomIdentifier.new!("!abc:example.org")}

  """
  @spec room_id(value :: any()) ::
          {:ok, Polyjuice.Util.Identifiers.V1.RoomIdentifer.t()}
          | {:error, :invalid_room_id, any()}
  def room_id(value) do
    with {:error, _} <- Polyjuice.Util.Identifiers.V1.RoomIdentifier.new(value) do
      {:error, :invalid_room_id, value}
    else
      {:ok, _} = ret -> ret
      _ -> {:error, :invalid_room_id, value}
    end
  end

  @doc """
  Function to match and parse room aliases.

  Intended to be used with `match/3`.  On success, returns `{:ok,
  parsed_value}`, where `parsed_value` is a
  `Polyjuice.Util.Identifiers.V1.RoomAliasIdentifier` struct.  On failure,
  returns `{:error, :invalid_room_alias, value}`.

  Examples:

      iex> Polyjuice.Util.Schema.match(
      ...>     "#room:example.org",
      ...>     &Polyjuice.Util.Schema.room_alias/1
      ...> )
      {:ok, Polyjuice.Util.Identifiers.V1.RoomAliasIdentifier.new!("#room:example.org")}

  """
  @spec room_alias(value :: any()) ::
          {:ok, Polyjuice.Util.Identifiers.V1.RoomAliasIdentifer.t()}
          | {:error, :invalid_room_alias, any()}
  def room_alias(value) do
    with {:error, _} <- Polyjuice.Util.Identifiers.V1.RoomAliasIdentifier.new(value) do
      {:error, :invalid_room_alias, value}
    else
      {:ok, _} = ret -> ret
      _ -> {:error, :invalid_room_alias, value}
    end
  end

  @doc """
  Function to match and parse event IDs.

  Intended to be used with `match/3`.  On success, returns `{:ok,
  parsed_value}`, where `parsed_value` is a
  `Polyjuice.Util.Identifiers.V4.EventIdentifier`,
  `Polyjuice.Util.Identifiers.V3.EventIdentifier`, or
  `Polyjuice.Util.Identifiers.V1.EventIdentifier` struct.  Note that some event
  IDs may be valid as both V4 and V3 event IDs; in this case, it will be
  returned as a V4 ID.  On failure, returns `{:error, :invalid_room_alias,
  value}`.

  Examples:

      iex> Polyjuice.Util.Schema.match(
      ...>     "$abc:example.org",
      ...>     &Polyjuice.Util.Schema.event_id/1
      ...> )
      {:ok, Polyjuice.Util.Identifiers.V1.EventIdentifier.new!("$abc:example.org")}

  """
  @spec event_id(value :: any()) ::
          {:ok, Polyjuice.Util.Identifiers.V1.RoomIdentifer.t()}
          | {:error, :invalid_event_id, any()}
  def event_id(value) do
    with {:error, _} <- Polyjuice.Util.Identifiers.V4.EventIdentifier.new(value),
         {:error, _} <- Polyjuice.Util.Identifiers.V3.EventIdentifier.new(value),
         {:error, _} <- Polyjuice.Util.Identifiers.V1.EventIdentifier.new(value) do
      {:error, :invalid_event_id, value}
    else
      {:ok, _} = ret -> ret
      _ -> {:error, :invalid_event_id, value}
    end
  end

  @doc """
  Function to match and parse server names.

  Intended to be used with `match/3`.  On success, returns `{:ok, value}`.  On
  failure, returns `{:error, :invalid_room_alias, value}`.

  Examples:

      iex> Polyjuice.Util.Schema.match(
      ...>     "example.org",
      ...>     &Polyjuice.Util.Schema.server_name/1
      ...> )
      {:ok, "example.org"}

  """
  @spec server_name(value :: any()) :: {:ok, String.t()} | {:error, :invalid_server_name, any()}
  def server_name(value) do
    if Polyjuice.Util.Identifiers.V1.ServerName.valid?(value) do
      {:ok, value}
    else
      :invalid_server_name
    end
  end

  @doc """
  Helper for matching enumerated values.

  Returns a function that can be used as a pattern.  If `values` is a list, the
  pattern matches if the parameter value is an item in the list.  If `values`
  is a map, the pattern matches if the parameter value is a key in the map, and
  the return value is the corresponding value in the map.

  `transform` is a function that transforms the parameter value before
  comparing it with `values`.  For example, to match in a case-insensitive
  manner, `&String.downcase/1` can be used as `transform`.

  Returns `{:error, :invalid_enum_value, allowed_values, value}` on failure.

  ## Examples

      iex> Polyjuice.Util.Schema.match(
      ...>     "f",
      ...>     Polyjuice.Util.Schema.enum(["f", "b"])
      ...> )
      {:ok, "f"}

      iex> Polyjuice.Util.Schema.match(
      ...>     "F",
      ...>     Polyjuice.Util.Schema.enum(["f", "b"])
      ...> )
      {:error, :invalid_enum_value, ["f", "b"], "F"}

      iex> Polyjuice.Util.Schema.match(
      ...>     "F",
      ...>     Polyjuice.Util.Schema.enum(
      ...>         %{"f" => :forward, "b" => :backward},
      ...>         &String.downcase/1
      ...>     )
      ...> )
      {:ok, :forward}

  """
  @spec enum(list() | map(), (any() -> any())) :: function()
  def enum(values, transform \\ & &1)

  def enum(values, transform) when is_list(values) do
    fn v ->
      Enum.find_value(values, {:error, :invalid_enum_value, values, v}, fn t ->
        if transform.(v) == t, do: {:ok, t}
      end)
    end
  end

  def enum(values, transform) when is_map(values) do
    fn v ->
      Enum.find_value(values, {:error, :invalid_enum_value, Map.keys(values), v}, fn {t, res} ->
        if transform.(v) == t, do: {:ok, res}
      end)
    end
  end

  @doc """
  Matches arrays of a given type.

  Returns a function that can be used as a pattern.  Matches if the parameter
  value is an array, and each item matches `pattern`.

  If any value does not match the pattern, returns the error given by the first
  value that does not match.

  Examples:

      iex> Polyjuice.Util.Schema.match([1, 2], Polyjuice.Util.Schema.array_of(:integer))
      {:ok, [1, 2]}

      iex> Polyjuice.Util.Schema.match([1, "foo"], Polyjuice.Util.Schema.array_of(:integer))
      {:error, :wrong_type, :integer, "foo"}

  """
  @spec array_of(any()) :: function()
  def array_of(pattern) do
    fn
      v, param when is_list(v) ->
        v = Enum.reverse(v)

        Enum.reduce_while(v, {:ok, []}, fn item, {:ok, acc} ->
          case match(item, pattern, param) do
            {:ok, val} -> {:cont, {:ok, [val | acc]}}
            err -> {:halt, err}
          end
        end)

      v, _ ->
        {:error, :wrong_type, :array, v}
    end
  end

  @doc """
  Matches objects whose keys and values match the given patterns.

  Returns a function that can be used as a pattern.  Matches if the parameter
  value is an object, whose keys match `key_pattern`, and whose values match
  `value_pattern`.

  If a key or value does not match its respective pattern, returns an error
  given by one of the non-matching keys or values.

  Examples:

      iex> Polyjuice.Util.Schema.match(
      ...>     %{"@alice:example.org" => 1},
      ...>     Polyjuice.Util.Schema.object_with_entries(
      ...>         &Polyjuice.Util.Schema.user_id/1, :integer
      ...>     )
      ...> )
      {:ok, %{
        Polyjuice.Util.Identifiers.V1.UserIdentifier.new!("@alice:example.org") => 1
      }}

  """
  @spec object_with_entries(any(), any()) :: function()
  def object_with_entries(key_pattern, value_pattern) do
    fn
      v, param when is_map(v) ->
        Enum.reduce_while(v, {:ok, %{}}, fn {key, val}, {:ok, acc} ->
          with {:ok, parsed_key} <- match(key, key_pattern, param),
               {:ok, parsed_val} <- match(val, value_pattern, param) do
            {:cont, {:ok, Map.put(acc, parsed_key, parsed_val)}}
          else
            err -> {:halt, err}
          end
        end)

      v, _ ->
        {:error, :wrong_type, :object, v}
    end
  end

  @doc """
  Matches objects whose values match a given pattern.

  Returns a function that can be used as a pattern.  Matches if the parameter
  value is an object, whose values match `value_pattern`.  Equivalent to
  `object_with_entries(:any, pattern)`.

  If a value does not match the pattern, returns an error given by one of the
  non-matching values.

  Examples:

      iex> Polyjuice.Util.Schema.match(
      ...>     %{"foo" => 1},
      ...>     Polyjuice.Util.Schema.object_with_values(:integer)
      ...> )
      {:ok, %{"foo" => 1}}

  """
  @spec object_with_values(any()) :: function()
  def object_with_values(pattern) do
    fn
      v, param when is_map(v) ->
        Enum.reduce_while(v, {:ok, %{}}, fn {key, val}, {:ok, acc} ->
          with {:ok, parsed_val} <- match(val, pattern, param) do
            {:cont, {:ok, Map.put(acc, key, parsed_val)}}
          else
            err -> {:halt, err}
          end
        end)

      v, _ ->
        {:error, :wrong_type, :object, v}
    end
  end

  @doc """
  Matches objects whose keys match a given pattern.

  Returns a function that can be used as a pattern.  Matches if the parameter
  value is an object, whose keys match `key_pattern`.  Equivalent to
  `object_with_entries(pattern, :any)`.

  If a key does not match the pattern, returns an error given by one of the
  non-matching keys.

  Examples:

      iex> Polyjuice.Util.Schema.match(
      ...>     %{"@alice:example.org" => 1},
      ...>     Polyjuice.Util.Schema.object_with_keys(
      ...>         &Polyjuice.Util.Schema.user_id/1
      ...>     )
      ...> )
      {:ok, %{
        Polyjuice.Util.Identifiers.V1.UserIdentifier.new!("@alice:example.org") => 1
      }}

  """
  @spec object_with_keys(any()) :: function()
  def object_with_keys(pattern) do
    fn
      v, param when is_map(v) ->
        Enum.reduce_while(v, {:ok, %{}}, fn {key, val}, {:ok, acc} ->
          with {:ok, parsed_key} <- match(key, pattern, param) do
            {:cont, {:ok, Map.put(acc, parsed_key, val)}}
          else
            err -> {:halt, err}
          end
        end)

      v, _ ->
        {:error, :wrong_type, :object, v}
    end
  end

  @doc """
  Matches values that match any of the given patterns.

  Examples:

      iex> Polyjuice.Util.Schema.match(
      ...>     1,
      ...>     Polyjuice.Util.Schema.any_of([:integer, :boolean])
      ...> )
      {:ok, 1}

      iex> Polyjuice.Util.Schema.match(
      ...>     true,
      ...>     Polyjuice.Util.Schema.any_of([:integer, :boolean])
      ...> )
      {:ok, true}

      iex> Polyjuice.Util.Schema.match(
      ...>     "string",
      ...>     Polyjuice.Util.Schema.any_of([:integer, :boolean])
      ...> )
      {:error, :no_match}
  """
  @spec any_of(list()) :: function()
  def any_of(patterns) do
    fn v, param ->
      Enum.find_value(patterns, {:error, :no_match}, fn pattern ->
        with {:ok, _} = ret <- match(v, pattern, param) do
          ret
        else
          _ -> nil
        end
      end)
    end
  end

  @doc """
  Matches the given pattern, but returns the unparsed value.

  This can be used, for example, if you want to match a user ID using
  `&user_id/1`, but want the raw string returned rather than the parsed
  value.

  Examples:

      iex> Polyjuice.Util.Schema.match(
      ...>     "@alice:example.org",
      ...>     Polyjuice.Util.Schema.unparsed(&Polyjuice.Util.Schema.user_id/1)
      ...> )
      {:ok, "@alice:example.org"}

  """
  @spec unparsed(any) :: function()
  def unparsed(pattern) do
    fn v, param ->
      case match(v, pattern, param) do
        {:ok, _} -> {:ok, v}
        _ -> :error
      end
    end
  end
end
